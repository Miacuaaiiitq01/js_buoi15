//Bài tính điểm học sinh
function btnKetQua() {
    var diemChuan = document.getElementById("diemChuan").value * 1;
    var diemToan = document.getElementById("diemToan").value * 1;
    var diemAnh = document.getElementById("diemAnh").value * 1;
    var diemVan = document.getElementById("diemVan").value * 1;
    var tongDiem = 0;
    tongDiem = diemToan + diemAnh + diemVan;
    var diemKhuVuc = document.getElementById("khuVuc").value * 1;
    var diemDoiTuong = document.getElementById("doiTuong").value * 1;
    tongDiem = tongDiem + diemKhuVuc + diemDoiTuong;
    if (tongDiem >= diemChuan) {
        document.getElementById(
            "result"
        ).innerText = `Bạn đã đậu với số điểm ${tongDiem}`;
    } else {
        document.getElementById(
            "resultDiemHocSinh"
        ).innerText = `Bạn chỉ có ${tongDiem} điểm rất tiếc không đậu`;
    }
}

//Bài tính tiền điện
function btnTinhTienDien() {
    const TU_50KW_DAU = 500;
    const TU_50KW_KE = 650;
    const TU_100KW_KE = 850;
    const TU_150KW_KE = 1100;
    const TU_150KW_TRO_DI = 1300;
    var tenNguoiDung = document.getElementById("tenNguoiDung").value;
    var soKW = document.getElementById("soKW").value * 1;
    var tongTienDien = 0;

    if (soKW <= 50) {
        tongTienDien = congThucTinh50Dau(soKW);
    } else if (50 < soKW && soKW <= 100) {
        tongTienDien = congThucTinh50Ke(soKW);
    } else if (100 < soKW && soKW <= 200) {
        tongTienDien = congThucTinh100Ke(soKW);
    } else if (200 < soKW && soKW <= 350) {
        tongTienDien = congThucTinh150Ke(soKW);
    } else {
        tongTienDien = congThucTinh150TroDi(soKW);
    }
    document.getElementById(
        "resultTinhTienDien"
    ).innerText = `Tiền điện của bạn là: ${tongTienDien} đồng`;
    function congThucTinh50Dau(soKW) {
        var congThuc = soKW * TU_50KW_DAU;
        return congThuc;
    }
    function congThucTinh50Ke(soKW) {
        var congThuc = 50 * TU_50KW_DAU + (soKW - 50) * TU_50KW_KE;
        return congThuc;
    }
    function congThucTinh100Ke(soKW) {
        var congThuc =
            50 * TU_50KW_DAU +
            (soKW - 50) * TU_50KW_KE +
            (soKW - 100) * TU_100KW_KE;
        return congThuc;
    }
    function congThucTinh150Ke(soKW) {
        var congThuc =
            50 * TU_50KW_DAU +
            (soKW - 50) * TU_50KW_KE +
            (soKW - 100) * TU_100KW_KE +
            (soKW - 200) * TU_150KW_KE;
        return congThuc;
    }
    function congThucTinh150TroDi(soKW) {
        var congThuc =
            50 * TU_50KW_DAU +
            (soKW - 50) * TU_50KW_KE +
            (soKW - 100) * TU_100KW_KE +
            (soKW - 200) * TU_150KW_KE +
            (soKW - 350) * TU_150KW_TRO_DI;
        return congThuc;
    }
}

//Bài tính thuế thu nhập cá nhân
function btnTinhThueThuNhapCaNhan() {
    const THUE_SUAT_DEN_60tr = 0.05;
    const THUE_SUAT_TU_60tr_DEN_120tr = 0.1;
    const THUE_SUAT_DEN_120tr_DEN_210tr = 0.15;
    const THUE_SUAT_DEN_210tr_DEN_384tr = 0.2;
    const THUE_SUAT_DEN_384tr_DEN_624tr = 0.25;
    const THUE_SUAT_DEN_624tr_DEN_960tr = 0.3;
    const THUE_SUAT_TREN_960tr = 0.35;
    var tongThuNhap = document.getElementById("thuNhapMotNam").value * 1;
    var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value * 1;
    var thuNhapChiuThue = tinhThuNhapChiuThue(tongThuNhap, soNguoiPhuThuoc);

    if (thuNhapChiuThue <= 60) {
        var thuNhapThucTe = tinhThuNhapThucTeDen60tr(thuNhapChiuThue);
    } else if (thuNhapChiuThue > 60 && thuNhapChiuThue <= 120) {
        var thuNhapThucTe = tinhThuNhapThucTeTu60trDen120tr(thuNhapChiuThue);
    } else if (thuNhapChiuThue > 120 && thuNhapChiuThue <= 210) {
        var thuNhapThucTe = tinhThuNhapThucTeTu120trDen210tr(thuNhapChiuThue);
    } else if (thuNhapChiuThue > 210 && thuNhapChiuThue <= 384) {
        var thuNhapThucTe = tinhThuNhapThucTeTu210trDen384tr(thuNhapChiuThue);
    } else if (thuNhapChiuThue > 384 && thuNhapChiuThue <= 624) {
        var thuNhapThucTe = tinhThuNhapThucTeTu384trDen624tr(thuNhapChiuThue);
    } else if (thuNhapChiuThue > 624 && thuNhapChiuThue <= 960) {
        var thuNhapThucTe = tinhThuNhapThucTeTu624trDen960tr(thuNhapChiuThue);
    } else {
        var thuNhapThucTe = tinhThuNhapThucTeTren960tr;
    }
    document.getElementById(
        "resultTinhThueThuNhapCaNhan"
    ).innerText = `Thu nhập của bạn sau thuế là: ${thuNhapThucTe} triệu`;
    function tinhThuNhapChiuThue(tongThuNhap, soNguoiPhuThuoc) {
        var thuNhapChiuThue = tongThuNhap - 4 - soNguoiPhuThuoc * 1.6;
        return thuNhapChiuThue;
    }
    function tinhThuNhapThucTeDen60tr(thuNhapChiuThue) {
        var thuNhapThuc = thuNhapChiuThue * THUE_SUAT_DEN_60tr;
        return thuNhapThuc;
    }
    function tinhThuNhapThucTeTu60trDen120tr(thuNhapChiuThue) {
        var thuNhapThuc =
            60 * THUE_SUAT_DEN_60tr +
            (thuNhapChiuThue - 60) * THUE_SUAT_TU_60tr_DEN_120tr;
        return thuNhapThuc;
    }
    function tinhThuNhapThucTeTu120trDen210tr(thuNhapChiuThue) {
        var thuNhapThuc =
            60 * THUE_SUAT_DEN_60tr +
            (thuNhapChiuThue - 60) * THUE_SUAT_TU_60tr_DEN_120tr +
            (thuNhapChiuThue - 120) * THUE_SUAT_DEN_120tr_DEN_210tr;
        return thuNhapThuc;
    }
    function tinhThuNhapThucTeTu210trDen384tr(thuNhapChiuThue) {
        var thuNhapThuc =
            60 * THUE_SUAT_DEN_60tr +
            (thuNhapChiuThue - 60) * THUE_SUAT_TU_60tr_DEN_120tr +
            (thuNhapChiuThue - 120) * THUE_SUAT_DEN_120tr_DEN_210tr +
            (thuNhapChiuThue - 210) * THUE_SUAT_DEN_210tr_DEN_384tr;
        return thuNhapThuc;
    }
    function tinhThuNhapThucTeTu384trDen624tr(thuNhapChiuThue) {
        var thuNhapThuc =
            60 * THUE_SUAT_DEN_60tr +
            (thuNhapChiuThue - 60) * THUE_SUAT_TU_60tr_DEN_120tr +
            (thuNhapChiuThue - 120) * THUE_SUAT_DEN_120tr_DEN_210tr +
            (thuNhapChiuThue - 210) * THUE_SUAT_DEN_210tr_DEN_384tr +
            (thuNhapChiuThue - 384) * THUE_SUAT_DEN_384tr_DEN_624tr;
        return thuNhapThuc;
    }
    function tinhThuNhapThucTeTu624trDen960tr(thuNhapChiuThue) {
        var thuNhapThuc =
            60 * THUE_SUAT_DEN_60tr +
            (thuNhapChiuThue - 60) * THUE_SUAT_TU_60tr_DEN_120tr +
            (thuNhapChiuThue - 120) * THUE_SUAT_DEN_120tr_DEN_210tr +
            (thuNhapChiuThue - 210) * THUE_SUAT_DEN_210tr_DEN_384tr +
            (thuNhapChiuThue - 384) * THUE_SUAT_DEN_384tr_DEN_624tr +
            (thuNhapChiuThue - 960) * THUE_SUAT_DEN_624tr_DEN_960tr;
        return thuNhapThuc;
    }
    function tinhThuNhapThucTeTren960tr(thuNhapChiuThue) {
        var thuNhapThuc =
            60 * THUE_SUAT_DEN_60tr +
            (thuNhapChiuThue - 60) * THUE_SUAT_TU_60tr_DEN_120tr +
            (thuNhapChiuThue - 120) * THUE_SUAT_DEN_120tr_DEN_210tr +
            (thuNhapChiuThue - 210) * THUE_SUAT_DEN_210tr_DEN_384tr +
            (thuNhapChiuThue - 384) * THUE_SUAT_DEN_384tr_DEN_624tr +
            (thuNhapChiuThue - 624) * THUE_SUAT_DEN_624tr_DEN_960tr +
            (thuNhapChiuThue - 960) * THUE_SUAT_TREN_960tr;
        return thuNhapThuc;
    }
}

//Bài tính tiền cáp

function tinhTienCap() {
    const PHI_XU_LY_HD_NHA_DAN = 4.5;
    const PHI_XU_LY_HD_DOANH_NGHIEP = 15;

    const PHI_DICH_VU_CO_BAN_NHA_DAN = 20.5;
    const PHI_DICH_VU_CO_BAN_DN = 75;

    const KENH_CAO_CAP_NHA_DAN = 7.5;
    const KENH_CAO_CAP_DN = 50;

    const PHI_KET_NOI_THEM = 5;

    var loaiKhachHang = document.getElementById("loaiKhachHang").value;
    var tinhKenhCaoCap = tinhKenhCaoCap();
    var phiDichVuCoBanDN = tinhPhiDichVuCoBanDN();
    var tongTienCap = tinhTienCap();
    document.getElementById(
        "resultTinhTienCap"
    ).innerHTML = `Tiền cáp: $ ${tongTienCap}`;

    function tinhTienCap() {
        var tienCap = 0;
        if (loaiKhachHang == "nhaDan") {
            tienCap =
                PHI_XU_LY_HD_NHA_DAN +
                PHI_DICH_VU_CO_BAN_NHA_DAN +
                tinhKenhCaoCap;
        } else {
            tienCap =
                PHI_XU_LY_HD_DOANH_NGHIEP + phiDichVuCoBanDN + tinhKenhCaoCap;
        }
        return tienCap;
    }
    function tinhPhiDichVuCoBanDN() {
        var soKetNoi = document.getElementById("soKetNoi").value * 1;
        if (soKetNoi > 10) {
            var tienPhiDichVuCoBanDN =
                (soKetNoi - 10) * PHI_KET_NOI_THEM + PHI_DICH_VU_CO_BAN_DN;
        } else {
            tienPhiDichVuCoBanDN = PHI_DICH_VU_CO_BAN_DN;
        }
        return tienPhiDichVuCoBanDN;
    }
    function tinhKenhCaoCap() {
        var kenhCaoCap = document.getElementById("kenhCaoCap").value * 1;
        if (loaiKhachHang == "nhaDan") {
            var tongKenhCaoCap = kenhCaoCap * KENH_CAO_CAP_NHA_DAN;
        } else {
            tongKenhCaoCap = kenhCaoCap * KENH_CAO_CAP_DN;
        }
        return tongKenhCaoCap;
    }
}

function showKetNoi() {
    var loaiKhachHang = document.getElementById("loaiKhachHang").value;
    if (loaiKhachHang == "doanhNghiep") {
        document.getElementById("chonDoanhNghiep").style.display = "block";
    } else {
        document.getElementById("chonDoanhNghiep").style.display = "none";
    }
}

